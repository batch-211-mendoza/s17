console.log("Hello World");

// Functions
	// Functions in JavaScript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

	// Function Declarations
		//(function statement) defines a function with the specified parameters

		/*
			Syntax:

			function functionName(){
				code block(statement);
			}
		*/
		// function keyword - used to define a JavaScript function
		// functionName - the function name. Function are named to be able to use later in the code.
		// function block ({}) - the statements which comprise the body of the function. This is where the code is to be executed.
			// we can assign a variable to hold a function, but that will be explained later

		function printName(){ //function declaration
			console.log("My name is John.");
		}
		printName(); // function invocation

		// *semi-colons are used to separate executable JavaScript statements

		// Function Invocation
			// The code block and statements inside a function is not imediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called
			// it is common to use the term "call a function" instead of "invoke a function"

		printName();

		// declaredFunction(); //error //much like variables, we cannot invoke a function we have yet to define

	// Function Declarations vs Expressions

		// Function Declarations
			// a function can be created through function declaration by using the function keyword and adding a function name
			// declared functions are not executed immediately. they are "saved for later use", and will be executed later, when they are invoked or called upon

			declaredFunction(); //decalred functions can be hoisted as long as the function has been defined
			function declaredFunction(){
				console.log("Hello World from declaredFunction()");
			}

			// Note: Hoisting is JavaScript's behavior for certain variables and functions to run or use them before their declaration

			declaredFunction();

			// Function Expression
				// a function can also be stored in a variable. This is called a function expression
				// a function expression is an anonymous function assigned to the variable function

				//Anonymous function - function without a name

				// variableFunction(); //error //because the function is stored in a variable
				/*
					error - function expressions, being stored in a let or const variable cannot be hoisted
				*/

				let variableFunction = function(){
					console.log("Hello Again");
				};
				variableFunction();

				// We can also create a function expression of a named function
				// However, to invoke the function expression, we invoke it by its variable name, not by its function name
				// Function expressions are always invoked/called using the variable name
				let funcExpression = function funcName() {
					console.log("Hello From The Other Side");
				};
				// funcName(); //error
				funcExpression();


				// declared functions and function expressions can be reassigned to new anonymous functions
				declaredFunction = function(){
					console.log("Updated declaredFunction()");
				}
				declaredFunction();

				funcExpression = function(){
					console.log("Updated funcExpression()");
				}
				funcExpression();

				const constantFunc = function(){
					console.log("Initialized with const!");
				}
				constantFunc();

				// constantFunc = function(){
				// 	console.log("Cannot be re-assigned!");
				// }
				// constantFunc(); //error //cannot reassign a function expression initialized with const
			
		// Function Scoping
		/*
			Scope is the accessibility (visibility) of variables within our program

			JavaScript variables has 3 types of scope:
			1. local/block scope
			2. global scope
			3. function scope
		*/
		//let globalVar = "Mr. Worldwide";
		{
			let localVar = "Armando Perez";
			console.log(localVar);
			//console.log(globalVar);
			// a global variable cannot be invoked inside a block if it's not declared before the code block
		}
		let globalVar = "Mr. Worldwide";
		console.log(globalVar);
		// console.log(localVar); //error
		// localVar being in a block, cannot be accessed outside of its code

		// Function Scope
		/*
			JavaScript has a function scope; Each function creates a new scope
			
			Variables  defined inside a function are not accessible (visible) from outside a function

			Variables declared with var, let, and const are quite similar when declared inside a function
		*/

		function showNames(){
			// Function scoped variables
			var functionVar = "Joe";
			const functionConst = "John";
			let functionLet = "Jane";

			console.log(functionVar);
			console.log(functionConst);
			console.log(functionLet);
		}
		showNames();
		// console.log(functionVar); //error
		// console.log(functionConst); //error
		// console.log(functionLet); //error
		/*
			the variables functionVar, functionConst, and functionLet are function scoped and cannot be accessed outside of the function they were declared in
		*/
		
	//Nested Functions
		// You can create another function inside a function
		// This is called  a nested function
	
		function myNewFunction(){
			let name = "Cee";

			function nestedFunction(){
				let nestedName = "Thor";
				console.log(name);
				console.log(nestedName);
			}
			// console.log(nestedName); //error //nestedName is not defined //nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in
			nestedFunction();
		}
		myNewFunction();
		// nestedFunction();
		/*
			since this function is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in
		*/

		// Function and Global Scoped Variables

		//Global scoped variable
		let globalName = "Nej";

		function myNewFunction2(){
			let nameInside = "Martin";
			//variables declared globally (outside any function) have global scope
			//global variables can be accessed from anywhere in a JavaScript program including from inside a function
			console.log(globalName);
		}
		myNewFunction2();

	// Using alert()
	// alert() allows us to show a small window at the top of our browser page to show information to our users
	// as opposed to a console.log() which will only show the message on the browser console, it allows us to show a short dialog or instruction to our user.
	// the page will wait until the user dismisses the dialog

	alert("Hello World"); //This will run immediately when the page loads

	/*
		alert() syntax:
		alert("<messageInString");
		//this can be done in number data type
	*/
	// You can also use an alert() to show a message to the user from a later function invocation

	function showSampleAlert(){
		alert("Hello, user");
	}
	showSampleAlert();

	//you will find that the page waits for the user to dismiss the alert dialog before proceeding
	console.log("I will only log in the console when the alert is dismissed.");

	//Notes on the use of alert();
		//show only an alert() for short dialogs/messages to the user
		//do not overuse alert() because the program/js has to wait for it to be dismissed before continuing

	//Using prompt()
		//prompt() allows us to show a small window at the top of our browser to gather user input
		//it, much like alert(), will have the page wait until the user completes or enters their input
		//the input from the prompt() will be returned as a String once the user dismisses the window

		let samplePrompt = prompt("Enter your name:");
		console.log("Hello, " + samplePrompt);

		/*
			prompt() syntax:

			prompt("<dialogInString");
		*/

		let sampleNullPrompt = prompt("Don't enter anything");
		console.log(sampleNullPrompt);
		//prompt() returns an empty string when submitted without any input
		//null if the user cancels the prompt()

		//prompt() can be used for us to gather user input and be used in our code
		//however, since prompt() windows will have the page wait until the user dismisses the window, it must not be overused

		//prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function

		function printWelcomeMessage(){
			let firstName = prompt("Enter Your First Name");
			let lastName = prompt("Enter Your Last Name");
			console.log("Hello, " + firstName + " " + lastName + "!");
			console.log("Welcome to my page!");
		}
		printWelcomeMessage();

	// Function Naming Conventions
		// function names should be definitive of the task it will perform. It usually contains a verb

		function getCourses(){
			let courses = ["Science 101", "Math 101", "English 101"];
			console.log(courses);
		}
		getCourses();

		//Avoid generic names to avoid confusion within your code

		function get(){
			let name = "Jamie";
			console.log(name);
		}
		get(); //do not use vague function names

		// Avoid pointless and inappropriate function names

		function pikachu(){
			console.log(25%5);
		}
		pikachu();

		//Name your 